// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "interactable.h" 
#include "Wall.generated.h"



UCLASS()
class SNAKE_API AWall : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWall();

	UPROPERTY(VisibleAnyWhere, EditDefaultsOnly)
		float Tel_X;

	UPROPERTY(VisibleAnyWhere, EditDefaultsOnly)
		float Tel_Y;

	UPROPERTY(VisibleAnyWhere, EditDefaultsOnly)
		float Tel_Z;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};

